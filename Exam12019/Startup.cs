using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Exam12019
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddHttpClient();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "Restaurant";
            });
            var cm = ConnectionMultiplexer.Connect("localhost");
            services.AddDataProtection().PersistKeysToStackExchangeRedis(cm, "RestaurantEncryptionKey");
            services.AddHttpContextAccessor();
            services.AddScoped<RestaurantServices>();
            services.AddScoped<ReservationServices>();
            services.AddScoped<AuthServices>();
            services.AddSession();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services
                .AddAuthentication("Login")
                .AddCookie("Login", option =>
                {
                    option.LoginPath = "/auth/login";
                    option.LogoutPath = "/auth/logout";
                    option.AccessDeniedPath = "/auth/denied";
                   
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
