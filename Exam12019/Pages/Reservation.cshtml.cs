using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Exam12019.Pages
{
    /// <summary>
    /// untuk menampilkan reservasi
    /// </summary>
    public class ReservationPageModel : PageModel
    {
        private readonly ReservationServices _Reservation;
        private readonly IHttpClientFactory _HttpFac;
        private readonly RestaurantServices _Restaurant;

        /// <summary>
        /// injeksi untuk reservasi
        /// </summary>
        /// <param name="reservationServices"></param>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public ReservationPageModel(ReservationServices reservationServices, RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Reservation = reservationServices;
            this._HttpFac = httpClientFactory;
            this._Restaurant = restaurantServices;
        }

        [TempData]
        public string SuccessMessage { set; get; }
        [BindProperty(SupportsGet = true)]
        public ReservationModel FormReservation { set; get; }
        [BindProperty(SupportsGet = true)]
        public RestaurantModel FormRestaurant { set; get; }
        [BindProperty]
        public List<RestaurantModel> Resev { set; get; }
        [BindProperty]
        public List<ReservationModel> Items { set; get; }
        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }

        /// <summary>
        /// mendapatkan dan menampilkan reservasi yang ada
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            //var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            //var client = _HttpFac.CreateClient();
            //var respond = await client.GetAsync(apiUrl);

            //if (respond.IsSuccessStatusCode == false)
            //{
            //    throw new Exception("Get by ID Failed");
            //}
            //var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            //FormRestaurant = new RestaurantModel
            //{
            //    RestaurantName = content.RestaurantName,
            //    RestaurantAddress = content.RestaurantAddress,
            //};
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var apiUrServe = "http://localhost:50508/api/restaurant/reservation/";
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);
            var respondReservation = await client.GetAsync(apiUrServe);
            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            Resev = await respond.Content.ReadAsAsync<List<RestaurantModel>>();
            Items = await respondReservation.Content.ReadAsAsync<List<ReservationModel>>();
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/reservation/" + IDFind;
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new ReservationModel
            {
                ID = IDFind
            });
            return RedirectToPage();//RedirectToPage("./reservationsearch/");
        }
    }
}