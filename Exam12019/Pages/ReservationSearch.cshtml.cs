using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Exam12019.Pages
{
    public class ReservationSearchModel : PageModel
    {
        private readonly ReservationServices _Reservation;
        private readonly IHttpClientFactory _HttpFac;
        private readonly RestaurantServices _Restaurant;

        public ReservationSearchModel(ReservationServices reservationServices, RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Reservation = reservationServices;
            this._HttpFac = httpClientFactory;
            this._Restaurant = restaurantServices;
        }

        [BindProperty(SupportsGet = true)]
        [Required]
        public Guid IDFind { set; get; }

        [BindProperty(SupportsGet = true)]
        public List<RestaurantModel> Resev { set; get; }
        [BindProperty(SupportsGet = true)]
        public List<ReservationModel> Items { set; get; }

        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrlResev = "http://localhost:50508/api/restaurant/reservation/";
            var apiUrl = "http://localhost:50508/api/restaurant/";

            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);
            var respondReservation = await client.GetAsync(apiUrlResev);
            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }
            Resev = await respond.Content.ReadAsAsync<List<RestaurantModel>>();
            Items = await respondReservation.Content.ReadAsAsync<List<ReservationModel>>();
            return Page();
        }

        public void OnPost()
        {
            //var apiUrl = "http://localhost:50508/api/restaurant/reservation/" + IDFind;
        }

        //<td>@Model.Items.Select(Q=>Q.ID).FirstOrDefault()</td>
        //<td>@Model.Items.Select(Q => Q.Name).FirstOrDefault()</td>
        //<td>@Model.Items.Select(Q => Q.Email).FirstOrDefault()</td>
        //<td>@Model.Resev.Where(Q=>Q.ID.ToString() == Model.Items.Select(X=>X.RestaurantID).ToString()).Select(Q=>Q.RestaurantName)</td>
        //<td>@Model.Resev.Where(Q => Q.ID.ToString() == Model.Items.Select(X => X.RestaurantID).ToString()).Select(Q => Q.RestaurantAddress)</td>

    }
}