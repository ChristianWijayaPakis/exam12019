using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Exam12019.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly AuthServices _AuthService;
        private readonly IConfiguration _Configuration;

        /// <summary>
        /// Model login yang dipakai.
        /// </summary>
        public class LoginFormModel
        {
            /// <summary>
            /// Ini untuk nama user.
            /// </summary>
            [Required]
            public string UserName { set; get; }
            /// <summary>
            /// Ini untuk password user.
            /// </summary>
            [Required]
            public string Password { set; get; }
        }
        /// <summary>
        /// Ini injection dari login service dan configuration.
        /// </summary>
        /// <param name="authServices"></param>
        /// <param name="configuration"></param>
        public LoginModel(AuthServices authServices, IConfiguration configuration)
        {
            this._AuthService = authServices;
            this._Configuration = configuration;
        }
        /// <summary>
        /// Contructor form.
        /// </summary>
        [BindProperty]
        public LoginFormModel Form { set; get; }

        /// <summary>
        /// Untuk memastikan kalau user sudah login atau belum. Kalau sudah user akan dilempar lagi ke index.
        /// </summary>
        /// <returns></returns>
        public IActionResult OnGet()
        {
           if(User.Identity.IsAuthenticated == true)
            {
                return RedirectToPage("/crud/Index");
            }

            return Page();


        }
        /// <summary>
        /// Method login agar user bisa masuk ke dalam web.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }
            //Membuat Bcrypt password.
            //var hash = BCrypt.Net.BCrypt.HashPassword("Accelist2019", 12); //_Configuration["accelist"]
            var valid = Form.UserName  == _Configuration["accelist"] && BCrypt.Net.BCrypt.Verify(Form.Password, _Configuration["AdminPassword"]);
            if (valid == false)
            {
                ModelState.AddModelError("Form.Password", "Invalid login email or password!");
                return Page();
            }

            //Ini untuk claim identity.
            var id = new ClaimsIdentity("Login");
            id.AddClaim(new Claim(ClaimTypes.Name, "Admin"));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.UserName)); // user ID / username / PK
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("Login", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToPage("/crud/Index");

        }
    }
}