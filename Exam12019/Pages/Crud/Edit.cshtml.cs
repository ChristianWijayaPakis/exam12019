using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Pages
{
    /// <summary>
    /// ini untuk edit data yang ada
    /// </summary>
    [Authorize]
    public class EditModel : PageModel
    {
        private readonly RestaurantServices _Restaurant;
        private readonly IHttpClientFactory _HttpFac;
        /// <summary>
        /// injeksi service ke edit
        /// </summary>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public EditModel(RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Restaurant = restaurantServices;
            this._HttpFac = httpClientFactory;
        }

        /// <summary>
        /// ini model untuk update
        /// </summary>
        public class UpdateFormModel {
            [Required]
            [StringLength(255)]
            public string UpdateName { set; get; }
            [Required]
            [StringLength(2000)]
            public string UpdateAddress { set; get; }
        }
        [BindProperty]
        public UpdateFormModel FormRestaurant { set; get; }
        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }

        /// <summary>
        /// mendapatkan data lama yang akan diupdate
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get by ID Failed");
            }
            var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            FormRestaurant = new UpdateFormModel
            {
                UpdateName = content.RestaurantName,
                UpdateAddress= content.RestaurantAddress,
            };

            return Page();
        }

        /// <summary>
        /// mengupdate data
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantModel
            {
                ID = IDFind,
                RestaurantName = FormRestaurant.UpdateName,
                RestaurantAddress = FormRestaurant.UpdateAddress
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Update via API Failed");
            }

            return RedirectToPage("./index");
        }
    }
}
