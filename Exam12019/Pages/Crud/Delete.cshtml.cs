using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Pages
{
    /// <summary>
    /// ini untuk delete, harus login terlebih dahulu
    /// </summary>
    [Authorize]
    public class DeleteModel : PageModel
    {
        private readonly RestaurantServices _Restaurant;
        private readonly IHttpClientFactory _HttpFac;
        /// <summary>
        /// Injection untuk delete
        /// </summary>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public DeleteModel(RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Restaurant = restaurantServices;
            this._HttpFac = httpClientFactory;
        }

        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }
        [BindProperty(SupportsGet = true)]
        public string RestaurantName { get; set; }
        /// <summary>
        /// mendapatkan id untuk didelete
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get by ID Failed");
            }

            var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            RestaurantName = content.RestaurantName;
            return Page();
        }

        /// <summary>
        /// redirect ke index setelah delete
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var response = await client.DeleteAsync(apiUrl);
            return RedirectToPage("./Index");
        }
    }
}
