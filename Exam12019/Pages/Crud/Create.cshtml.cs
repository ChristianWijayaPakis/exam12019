using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Pages
{
    /// <summary>
    /// Ini untuk membuat restaurant.
    /// </summary>
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly RestaurantServices _Restaurant;
        private readonly IHttpClientFactory _HttpFac;

        /// <summary>
        /// Ini injection dari RestaurantServices.
        /// </summary>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public CreateModel(RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Restaurant = restaurantServices;
            this._HttpFac = httpClientFactory;
        }
        
        [TempData]
        public string SuccessMessage { set; get; }
        /// <summary>
        /// Ini model untuk membuat Data Restoran
        /// </summary>
        public class CreateFormModel {
            public Guid ID { set; get; }
            [Required]
            [StringLength(255)]
            public string CreateNama { set; get; }
            [Required]
            [StringLength(255)]
            public string CreateAddress { set; get; }
        }

        /// <summary>
        /// Membuat model baru untuk bind property
        /// </summary>
        [BindProperty]
        public CreateFormModel FormCreate { set; get; }
        
        public void OnGet()
        {
        }
        /// <summary>
        /// Untuk membuat restaaurant baru
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var client = _HttpFac.CreateClient();

            var tempRestaurant = await _Restaurant.GetRestaurantsAsync();

            if (tempRestaurant.Where(Q => Q.RestaurantName == FormCreate.CreateNama)
                .Select(Q => Q.RestaurantName).FirstOrDefault() == FormCreate.CreateNama)
            {
                ModelState.AddModelError("FormRestaurant.RestaurantName", "Nama Sudah ada copyright, lu gabisa ambil merk orang, yang bener aja user");
                return Page();
            }

            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantModel
            {
                ID = Guid.NewGuid(),
                RestaurantName = FormCreate.CreateNama,
                RestaurantAddress = FormCreate.CreateAddress
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Insert via API Failed");
            }
            SuccessMessage = "Insert Berhasil";
            TempData["ErrorMessage"] = "Insert Gagal";

            return RedirectToPage("./index");
        }
    }
}
