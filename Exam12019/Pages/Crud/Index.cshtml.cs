using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Pages
{
    /// <summary>
    /// untuk menampilkan data
    /// </summary>
    public class IndexModel : PageModel
    {
        private readonly RestaurantServices _Restaurant;
        private readonly IHttpClientFactory _HttpFac;
        /// <summary>
        /// injeksi service
        /// </summary>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public IndexModel(RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory)
        {
            this._Restaurant = restaurantServices;
            this._HttpFac = httpClientFactory;
        }
        [BindProperty]
        public List<RestaurantModel> Resto{ set; get; }
        /// <summary>
        /// mendapatkan data dan menampilkannya
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/";
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            Resto = await respond.Content.ReadAsAsync<List<RestaurantModel>>();
            return Page();
        }
    }
}