using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Exam12019.Pages
{
    /// <summary>
    /// ini untuk reservasi
    /// </summary>
    public class InsertReservationModel : PageModel
    {
        private readonly ReservationServices _Reservation;
        private readonly IHttpClientFactory _HttpFac;
        private readonly RestaurantServices _Restaurant;
        private readonly IConfiguration _Configuration;

        /// <summary>
        /// injeksi insert reservasi
        /// </summary>
        /// <param name="reservationServices"></param>
        /// <param name="restaurantServices"></param>
        /// <param name="httpClientFactory"></param>
        public InsertReservationModel(ReservationServices reservationServices, RestaurantServices restaurantServices, IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this._Reservation = reservationServices;
            this._HttpFac = httpClientFactory;
            this._Restaurant = restaurantServices;
            this._Configuration = configuration;


        }

        [TempData]
        public string SuccessMessage { set; get; }
        [BindProperty(SupportsGet = true)]
        public ReservationModel FormReservation { set; get; }
        [BindProperty(SupportsGet = true)]
        public RestaurantModel FormRestaurant { set; get; }
        [BindProperty]
        public List<ReservationModel> Resev { set; get; }
        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }

        public async Task<IActionResult> OnGetAsync()
        {
            //var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            //var client = _HttpFac.CreateClient();
            //var respond = await client.GetAsync(apiUrl);

            //if (respond.IsSuccessStatusCode == false)
            //{
            //    throw new Exception("Get by ID Failed");
            //}
            //var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            //FormRestaurant = new RestaurantModel
            //{
            //    RestaurantName = content.RestaurantName,
            //    RestaurantAddress = content.RestaurantAddress,
            //};

            return Page();
        }
        /// <summary>
        /// post reservasi di restoran
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/reservation/";
            var client = _HttpFac.CreateClient();

            Resev = await _Reservation.GetReservationAsync();

            if(Resev.Where(Q=>Q.Name == FormReservation.Name)
                .Select(Q=>Q.Name).FirstOrDefault() == FormReservation.Name)
            {
                ModelState.AddModelError("FormReservation.Name", "Nama sudah ada bro, jangan rakus");
                return Page();
            }


            //testing
            var gizmo = new ReservationModel
            {
                ID = Guid.NewGuid(),
                Name = FormReservation.Name,
                Email = FormReservation.Email,
                Time = FormReservation.Time,
                RestaurantID = IDFind
            };
            //Uri gizmoUri = null;
            MediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();
            HttpContent content = new ObjectContent<ReservationModel>(gizmo, jsonFormatter);
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri("http://localhost:50508/api/restaurant/reservation/"),
                Method = HttpMethod.Post,
                Content = content
            };
            request.Headers.Add("ApiKey", _Configuration["ReservationApiKey"]);

            var response2 = client.SendAsync(request).Result;
            //testing

            //var response = await client.PostAsJsonAsync(apiUrl, new ReservationModel
            //{
            //    ID = Guid.NewGuid(),
            //    Name = FormReservation.Name,
            //    Email = FormReservation.Email,
            //    Time = FormReservation.Time,
            //    RestaurantID = IDFind
            //});

            //if (response.IsSuccessStatusCode == false)
            //{
            //    throw new Exception("Reservation Failed");
            //}

            if (response2.IsSuccessStatusCode == false)
            {
                throw new Exception("Reservation Failed");
            }
                return RedirectToPage("./reservation");
        }
    }
}