﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.ViewModel
{
    public class AuthModel
    {
        /// <summary>
        /// Ini untuk nama User.
        /// </summary>
        public string UserName { set; get; }
        /// <summary>
        /// Ini untuk password user.
        /// </summary>
        public string Password { set; get; }
        /// <summary>
        /// Ini untuk role user.
        /// </summary>
        public string Role { set; get; }
    }
}
