﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.ViewModel
{
    public class ReservationModel
    {
        /// <summary>
        /// Ini untuk ID Reservation.
        /// </summary>
        public Guid ID { set; get; }
        /// <summary>
        /// Ini untuk Name Reservation.
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// Ini untuk Email Reservation.
        /// </summary>
        public string Email { set; get; }
        /// <summary>
        /// Ini untuk Time Reservation.
        /// </summary>
        public DateTimeOffset Time { set; get; }
        /// <summary>
        /// Ini untuk RestaurantID di Reservation.
        /// </summary>
        public Guid RestaurantID { set; get; }
    }
}
