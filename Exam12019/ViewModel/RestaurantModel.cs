﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.ViewModel
{
    public class RestaurantModel
    {
        /// <summary>
        /// Ini untuk ID restaurant.
        /// </summary>
        public Guid ID { set; get; }
        /// <summary>
        /// Ini untuk Nama Restaurant.
        /// </summary>
        public string RestaurantName { set; get; }
        /// <summary>
        /// Ini untuk Address restaurant.
        /// </summary>
        public string RestaurantAddress { set; get; }
    }
}
