﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    /// <summary>
    /// API ini untuk service Reservation
    /// </summary>
    [Route("api/restaurant/reservation")]
    public class RestaurantReservationApiController : Controller
    {
        private readonly ReservationServices _Reservation;
        private readonly IConfiguration _Configuration;

        [BindProperty]
        public ReservationModel FormReservation { set; get; }

        /// <summary>
        /// Injection dari reservation service.
        /// </summary>
        /// <param name="reservationServices"></param>
        /// <param name="configuration"></param>
        public RestaurantReservationApiController(ReservationServices reservationServices, IConfiguration configuration)
        {
            this._Reservation = reservationServices;
            this._Configuration = configuration;
        }

        /// <summary>
        /// Ini untuk memasukan data reservation.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "insert-reservation")]
        public async Task<ActionResult> Post([FromBody]ReservationModel model)
        {
            var apiKey = Request.Headers["ApiKey"];
            if(string.IsNullOrEmpty(apiKey) || apiKey!=_Configuration["ReservationApiKey"])
            {
                return BadRequest("Invalid API Key");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _Reservation.CreateReservationAsync(model);
            return Ok();
        }

        /// <summary>
        /// Ini untuk menampilkan semua data reservation.
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "get-all-data-resev")]
        public async Task<ActionResult<List<ReservationModel>>> GetAllData()
        {
            
            var allData = await _Reservation.GetReservationAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        /// <summary>
        /// Ini untuk menampilkan data reservation berdasarkan ID nya.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "get-data-by-id-resev")]
        public async Task<ActionResult<ReservationModel>> GetDataById(Guid id)
        {

            var allData = await _Reservation.GetReservationAsync();
            var findData = allData.Where(Q => Q.ID == id).FirstOrDefault();

            
            if (findData == null)
            {
                return NotFound();
            }

            return findData;
        }
    }
}
