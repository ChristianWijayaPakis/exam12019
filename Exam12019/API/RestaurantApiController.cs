﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Services;
using Exam12019.ViewModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    /// <summary>
    /// API ini untuk Restaurant service
    /// </summary>
    [ApiController]
    [Route("api/restaurant/")]
    public class RestaurantApiController : Controller
    {
        private readonly RestaurantServices _RestaurantServiceList;
        
        [BindProperty]
        public RestaurantModel FormRestaurant { set; get; }

        /// <summary>
        /// Injection untuk restaurant service.
        /// </summary>
        /// <param name="restaurantServices"></param>
        public RestaurantApiController(RestaurantServices restaurantServices)
        {
            this._RestaurantServiceList = restaurantServices;
        }
        /// <summary>
        /// Untuk create restaurant.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "insert-restaurants")]
        public async Task<ActionResult> Post([FromBody]RestaurantModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _RestaurantServiceList.CreateRestaurantsAsync(model);
            return Ok();
        }
        /// <summary>
        /// Untuk update restaurant.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id}", Name = "update-restaurant")]
        public async Task<ActionResult> Update([FromBody]RestaurantModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantServiceList.GetRestaurantsAsync();
            var findData = allData.FindIndex(Q => Q.ID == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData[findData] = model;

            await _RestaurantServiceList.UpdateRestaurantsAsync(allData);
            return Ok();
        }

        /// <summary>
        /// Untuk delete Restaurant.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}", Name = "delete-restaurant")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantServiceList.GetRestaurantsAsync();
            var findData = allData.FindIndex(Q => Q.ID == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData.RemoveAt(findData);

            await _RestaurantServiceList.UpdateRestaurantsAsync(allData);
            return Ok();
        }
        /// <summary>
        /// Untuk menampilkan semua data .
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "get-all-data")]
        public async Task<ActionResult<List<RestaurantModel>>> GetAllData()
        {
            var allData = await _RestaurantServiceList.GetRestaurantsAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }
        /// <summary>
        /// Untuk menampilkan data berdasarkan ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "get-data-by-id")]
        public async Task<ActionResult<RestaurantModel>> GetDataById(Guid id)
        {
            var allData = await _RestaurantServiceList.GetRestaurantsAsync();
            var findData = allData.Where(Q => Q.ID == id).FirstOrDefault();

            if (findData == null)
            {
                return NotFound();
            }

            return findData;
        }
    }
}
