﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.ViewModel;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Exam12019.Services
{
    public class RestaurantServices
    {
        private readonly IDistributedCache Cache;
        /// <summary>
        /// inisialisasi pemakaian cache
        /// </summary>
        /// <param name="distributedCache"></param>
        public RestaurantServices(IDistributedCache distributedCache)
        {
            Cache = distributedCache;
        }
        /// <summary>
        /// membuat restoran baru dan memasukannya ke redis cache
        /// </summary>
        /// <param name="restaurantModel"></param>
        /// <returns></returns>
        public async Task CreateRestaurantsAsync(RestaurantModel restaurantModel)
        {
            var products = await GetRestaurantsAsync();
            if (products == null)
            {
                products = new List<RestaurantModel>();
            }

            products.Add(restaurantModel);
            var restaurantsJson = JsonConvert.SerializeObject(products);
            await Cache.SetStringAsync("Restaurants", restaurantsJson);
        }

        /// <summary>
        /// mendapatkan data restoran dari redis cache
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantModel>> GetRestaurantsAsync()
        {
            var valueJson = await Cache.GetStringAsync("Restaurants");
            if (valueJson == null)
            {
                return new List<RestaurantModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantModel>>(valueJson);
        }
     
        /// <summary>
        /// mengupdate data di rediss cache
        /// </summary>
        /// <param name="restaurants"></param>
        /// <returns></returns>
        public async Task UpdateRestaurantsAsync(List<RestaurantModel> restaurants)
        {
            var json = JsonConvert.SerializeObject(restaurants);
            await Cache.SetStringAsync("Restaurants", json);
        }
    }
}

