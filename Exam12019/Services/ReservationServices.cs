﻿using Exam12019.ViewModel;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class ReservationServices
    {
        private readonly IDistributedCache Cache;
        /// <summary>
        /// inisialisasi cache
        /// </summary>
        /// <param name="distributedCache"></param>
        public ReservationServices(IDistributedCache distributedCache)
        {
            Cache = distributedCache;
        }

        /// <summary>
        /// membuat reservasi baru dan memasukannnya ke cache
        /// </summary>
        /// <param name="reservationModel"></param>
        /// <returns></returns>
        public async Task CreateReservationAsync(ReservationModel reservationModel)
        {
            var reservations = await GetReservationAsync();

            if (GetRestaurantsAsync() != null)
            {
                if (reservations == null)
                {
                    reservations = new List<ReservationModel>();
                }
                reservations.Add(reservationModel);
                var reservationsJson = JsonConvert.SerializeObject(reservations);
                await Cache.SetStringAsync("Reservations", reservationsJson);
            }
        }

        /// <summary>
        /// mendapatkan data restoran dari redis cache
        /// </summary>
        /// <returns></returns>
        public async Task<List<RestaurantModel>> GetRestaurantsAsync()
        {
            var valueJson = await Cache.GetStringAsync("Restaurants");
            if (valueJson == null)
            {
                return new List<RestaurantModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantModel>>(valueJson);
        }

        /// <summary>
        /// mendapatkan data reservasi dari redis cache
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReservationModel>> GetReservationAsync()
        {
            var valueJson = await Cache.GetStringAsync("Reservations");
            if (valueJson == null)
            {
                return new List<ReservationModel>();
            }
            return JsonConvert.DeserializeObject<List<ReservationModel>>(valueJson);
        }
    }
}
