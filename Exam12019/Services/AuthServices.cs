﻿using Exam12019.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    /// <summary>
    /// service untuk auth jika ada registrasi
    /// </summary>
    public class AuthServices
    {
        public List<AuthModel> Auth { set; get; } = new List<AuthModel>();
    }
}
